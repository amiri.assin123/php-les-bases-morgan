<?php

// Récupérer les paramètres avec argv
// Convertir le argv en string (tableau -> string)
// Réduire à un seul espace entre les mots
// Retirer les espaces au début et fin de chaine
// Pas de tabulation ou autre (\n)
// Retirer la ligne de code (comme l'exercice 4 liée à la key 0)

if ($argc != 2) {
    exit();
}
    $phraseinitiale = $argv[1];

    $removecharacter = ('/\s+/');
    $newcharacter = (' ');
    $nouvellephrase = preg_replace($removecharacter, $newcharacter, $phraseinitiale);

    echo trim($nouvellephrase, ' ');
    echo "\n";
