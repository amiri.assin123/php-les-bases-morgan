<?php

if ($_GET['action'] == 'set') {
    // Si l'action récupérée par le $_get est 'set' alors..
    if ($_GET['name'] && $_GET['value']) {
        // On récupère les valeurs des clefs plat et value
        setcookie($_GET['name'], $_GET['value'], time() + 3600);
        // Puis on les associe à un cookie grâce à la fonction setcookie
        // Et on lui donne un temps d'existence de 3600 secondes soit une heure
    }
}

if ($_GET['action'] == 'get') {
    // Si l'action récupérée par le $_get est get alors..
    if (isset($_COOKIE[$_GET['name']])) {
        // On vérifie qu'il y a bien une valeur associée à la clef name
        echo $_COOKIE[$_GET['name']] . "\n";
        // Et on l'affiche
    }
}

if ($_GET['action'] == 'del') {
    // Si l'action récupérée par le $_get est del alors..
    setcookie($_GET['name'], $_GET['value'], time() - 3600);
    // On diminue le temps d'existence du cookie de 3600 secondes, càd on le supprime
}
